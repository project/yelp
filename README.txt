
Yelp Module for Drupal 6.x

This module allows you to display business listings on your drupal site, which are retrieved from Yelp.com using the Yelp API.
Initially the module displays a photo of the business, title, rating and address. 
The display is themeable, and there is alot more information you can display. (such are reviews, additional business information, nearby links, etc).

To use this module you need to have a  yelp API key. which you can get for free from:
http://www.yelp.com/developers/getting_started/api_access

** This module it not an offical Yelp.com module and has no connection to yelp.com **
** As such, all results, returned data, photos & images are registered information of yelp.com **